import Foundation

extension Array where Element == String {

  func dictionary(using separator: String) -> [String: String] {
    return self.reduce(into: [:], { result, next in
      let item = next.components(separatedBy: separator)
      result[ item[0].trimmingCharacters(in: .whitespacesAndNewlines) ] = item[1].trimmingCharacters(in: .whitespacesAndNewlines)
    })
  }

}
