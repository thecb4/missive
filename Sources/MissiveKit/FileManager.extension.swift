import Foundation

extension FileManager {

  public var currentDirectoryURL: URL {
    return URL(fileURLWithPath: currentDirectoryPath)
  }

  public static func makeFileURL(using path: String, relativeTo directory: URL = FileManager.default.currentDirectoryURL) -> URL {

    // print("relative to: \(directory)")
    // if #available(macOS 10.12, *) {
    //   return URL(fileURLWithPath: path, relativeTo: directory)
    // } else {
    //   return URL(fileURLWithPath: path)
    // }
    return URL(fileURLWithPath: path)

  }

  public static func directory(of path: String) -> String {
    let components = path.components(separatedBy: "/")
    let directory = components[0..<components.count-2].joined(separator: "/")
    print(directory)
    return directory
  }
}
