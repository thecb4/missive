import Foundation

extension Dictionary {

  public func encoded() throws -> Data {
    return try JSONSerialization.data(withJSONObject: self as NSDictionary, options: .prettyPrinted)
  }

}
