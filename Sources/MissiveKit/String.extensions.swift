import Foundation

extension String {

  public func stripFrontMatter() -> String {
    return self.strip(matching: FrontMatter.pattern + "\\n")
  }

  public func extractFrontMatter() throws -> FrontMatter {

    // https://stackoverflow.com/questions/26963379/remove-whitespace-character-set-from-string-excluding-space
    // https://stackoverflow.com/questions/9756392/python-regex-to-match-yaml-front-matter
    // https://learnappmaking.com/regular-expressions-swift-string/
    // https://benscheirman.com/2014/06/regex-in-swift/
    // https://stackoverflow.com/questions/29784447/swift-regex-does-a-string-match-a-pattern
    // https://regexr.com
    let options: NSRegularExpression.Options = [.caseInsensitive]

    guard let results = try? self.match(pattern: FrontMatter.pattern, options: options) else {
      return FrontMatter()
    }

    guard let actualString = results.first else {
      return FrontMatter()
    }

    let matters = actualString.strip(matching: "---")

    // https://www.hackingwithswift.com/example-code/strings/how-to-trim-whitespace-in-a-string
    let filtered = matters.components(separatedBy: "\n").filter {
      $0.trimmingCharacters(in: NSCharacterSet.whitespaces) != ""
    }

    let matter_dictionary = filtered.dictionary(using: ":")

    // https://stackoverflow.com/questions/29625133/convert-dictionary-to-json-in-swift
    guard let matterData = try? matter_dictionary.encoded() else {
      throw "Could not encode front matter to data"
    }

    guard let frontMatter: FrontMatter = try? matterData.decoded() else {
      throw "Could not decode to Front Matter struct"
    }

    return frontMatter

  }

  func match(pattern: String, options: NSRegularExpression.Options) throws -> [String] {

    let regex = try NSRegularExpression(pattern: pattern, options: options)

    let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))

    return matches.compactMap { Range($0.range, in: self).map { String(self[$0]) } }
  }

  func strip(matching pattern: String) -> String {
    // return self.replacingOccurrences(of: pattern, with: "")
    return self.replacingOccurrences(of: pattern, with: "", options: .regularExpression, range: nil)
  }

}
