import SwiftMarkdown

public struct Missive {

  public static let defaultHTMLFile: String = "missive.html"

  public init() {}

  func html5Template(using content: String, with matter: FrontMatter? = nil) -> String {
    return
      """
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="UTF-8">
      <style type="text/css">
      pre {
          font-family: "Courier 10 Pitch", Courier, monospace;
          font-size: 95%;
          line-height: 140%;
          white-space: pre;
          white-space: pre-wrap;
          white-space: -moz-pre-wrap;
          white-space: -o-pre-wrap;
      }

      code {
          font-family: Monaco, Consolas, "Andale Mono", "DejaVu Sans Mono", monospace;
          font-size: 95%;
          line-height: 140%;
          white-space: pre;
          white-space: pre-wrap;
          white-space: -moz-pre-wrap;
          white-space: -o-pre-wrap;
          background: #faf8f0;
      }

      #content code {
          display: block;
          padding: 0.5em 1em;
          border: 1px solid #bebab0;
      }
      </style>
      <title>\(matter?.title ?? "")</title>
      </head>
      <body>
      \(content)
      </body>
      </html>

      """
  }

  public func html(from source: String, with matter: FrontMatter? = nil) throws -> String {

    let content = try render(source)

    return html5Template(using: content, with: matter)

  }

  public func render(_ markdown: String) throws -> String {
    let content = try markdownToHTML(markdown, options: [])
    return content
  }

}
