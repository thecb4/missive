import Foundation

public struct Configuration {
  let draftsDirectory: String
  let postsDirectory: String
  let templatesDirectory: String
  let indexMarkdownFile: String
  let indexHTMLFile: String

  public static let defaultDraftsDirectory = "drafts"
  public static let defaultPostsDirectory = "public/posts"
  static let defaultsTemplatesDirectory = "templates"
  static let defaultIndexMarkdownFile = "index.md"
  static let defaultIndexHTMLFile = "index.html"

  init(
    draftsDirectory: String = Configuration.defaultDraftsDirectory,
    postsDirectory: String = Configuration.defaultPostsDirectory,
    templatesDirectory: String = Configuration.defaultsTemplatesDirectory,
    indexMarkdownFile: String = Configuration.defaultIndexMarkdownFile,
    indexHTMLFile: String = Configuration.defaultIndexHTMLFile
  ) {
    self.draftsDirectory = draftsDirectory
    self.postsDirectory = postsDirectory
    self.templatesDirectory = templatesDirectory
    self.indexMarkdownFile = indexMarkdownFile
    self.indexHTMLFile = indexHTMLFile
  }
}
