import Foundation
import DarkMatter

public struct FrontMatter: DarkMatter {

  public static let pattern = "^---[\\s\\S]+?---"

  public let title: String?
  public let layout: String?

  public init(title: String? = nil, layout: String? = nil) {
    self.title = title
    self.layout = layout
  }

}
