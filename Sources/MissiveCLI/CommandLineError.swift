import Foundation

enum CommandLineError: Error {
  case missingFileName
  case badWorkDirectoryName(String)
  case badDraftsDirectoryName(String)

  var description: String {
    switch self {
      case .missingFileName: return "Bad missive markdown path passed as argument"
      case .badWorkDirectoryName(let dir): return "Missive working directory '\(dir)' does not exist, please create"
      case .badDraftsDirectoryName(let dir): return "Missive drafts directory '\(dir)' does not exist, please create"
    }
  }
}
