// import Foundation
// import MissiveKit
//
// struct SingleMarkdownFileCommand {
//
//   var writePath = FileManager.default.currentDirectoryURL
//
//   public init(writePath: URL = FileManager.default.currentDirectoryURL) {
//     self.writePath = writePath
//   }
//
//   func validated(argument: String?) throws -> FileURL {
//
//     // let currentDirectoryURL = Foundation.URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
//
//     guard let file = argument else {
//       throw "Could not obtain file name"
//     }
//
//     let path = FileManager.makeFileURL(using: file)
//
//     if path.pathExtension == "md" || path.pathExtension == "markdown" {
//
//       return path
//
//     } else {
//
//       throw "Validation error. Bad path for markdown \(path)"
//
//     }
//
//   }
//
//   @available(macOS 10.12, *)
//   func exec(using argument: String?) throws {
//     let path = try validated(argument: argument)
//     let source = try String(contentsOf: path)
//     let matter = try source.extractFrontMatter()
//     let missive = Missive()
//     let html = try missive.html(from: source.stripFrontMatter(), with: matter)
//     let toPath = FileManager.makeFileURL(using: Missive.defaultHTMLFile, relativeTo: writePath)
//     try html.write(to: toPath, atomically: false, encoding: .utf8)
//   }
//
// }
