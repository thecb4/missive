import Foundation
import Utility
import MissiveKit

// https://github.com/apple/swift-package-manager/blob/master/Sources/Utility/ArgumentParser.swift

typealias FileURL = Foundation.URL

public class MissiveCommandLine {

  var registry = CommandRegistry(commandName: "missive", usage: "<command> <options>", overview: "Write blog posts in markdown, publish to html\n")

  public init() {

    registry.register(command: DraftCommand.self)
    registry.register(command: PublishCommand.self)

  }

  public func run() {

    registry.run()

  }

}
