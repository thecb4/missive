//
//  Command.swift
//  MissiveCLI
//
//  Created by Cavelle Benjamin on 19-Jan-09 (02).
//

import Foundation
import Utility
import MissiveKit

// https://www.enekoalonso.com/articles/handling-commands-with-swift-package-manager

protocol Command {
  var command: String { get }
  var overview: String { get }

  init(parser: ArgumentParser)

  func run(with arguments: ArgumentParser.Result) throws
}
