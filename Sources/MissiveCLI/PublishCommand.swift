import Foundation
import Utility
import Basic
import MissiveKit

struct PublishCommand: Command {

    let command = "publish"
    let overview = "Publish a markdown formatted post to html"

    private let fileNameArgument: PositionalArgument<String>
    private let workDirOption: OptionArgument<String>
    private let draftsDirOption: OptionArgument<String>
    private let postsDirOption: OptionArgument<String>

    enum Month: Int {
      case jan = 1
      case feb = 2
      case mar = 3
      case apr = 4
      case may = 5
      case jun = 6
      case jul = 7
      case aug = 8
      case sep = 9
      case oct = 10
      case nov = 11
      case dec = 12

      var string: String {
        switch self {
          case .jan: return "Jan"
          case .feb: return "Feb"
          case .mar: return "Mar"
          case .apr: return "Apr"
          case .may: return "May"
          case .jun: return "Jun"
          case .jul: return "Jul"
          case .aug: return "Aug"
          case .sep: return "Sep"
          case .oct: return "Oct"
          case .nov: return "Nov"
          case .dec: return "Dec"
        }
      }
    }

    init(parser: ArgumentParser) {
      let subparser = parser.add(subparser: command, overview: overview)
      fileNameArgument = subparser.add(positional: "filename", kind: String.self, usage: "Name of the markdown file")
      workDirOption    = subparser.add(option: "--directory", shortName: "-wd", kind: String.self, usage: "Working directory for missive")
      draftsDirOption  = subparser.add(option: "--drafts", shortName: "-dd", kind: String.self, usage: "Drafts directory for missive")
      postsDirOption  = subparser.add(option: "--posts", shortName: "-ps", kind: String.self, usage: "Posts directory for missive")

    }

    func fileName(_ arguments: ArgumentParser.Result) throws -> [String] {

      guard let name = arguments.get(fileNameArgument) else {

        throw CommandLineError.missingFileName

      }

      return [name]

    }

    func workingDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      if let dir = arguments.get(workDirOption) {
          if FileManager.default.fileExists(atPath: dir) {
            return [dir]
          } else {
            throw CommandLineError.badWorkDirectoryName(dir)
          }
      } else {
        return []
      }

    }

    func draftsDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      if let dir = arguments.get(draftsDirOption) {

        if FileManager.default.fileExists(atPath: dir) {
          return [dir]
        } else {
          throw CommandLineError.badDraftsDirectoryName(dir)
        }
      } else {
        return [Configuration.defaultDraftsDirectory]
      }
    }

    func postsDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      if let dir = arguments.get(draftsDirOption) {
        // print("draftsDir = \(draftsDir)")
        if FileManager.default.fileExists(atPath: dir) {
          return [dir]
        } else {
          throw CommandLineError.badDraftsDirectoryName(dir)
        }
      } else {
        return [Configuration.defaultPostsDirectory]
      }
    }

    func dateDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      let date = Date()
      let calendar = Calendar.current

      let year    = calendar.component(.year, from: date)
      let month    = calendar.component(.month, from: date)
      let day    = calendar.component(.day, from: date)
      // let hour    = calendar.component(.hour, from: date)
      // let minutes = calendar.component(.minute, from: date)
      // let seconds = calendar.component(.second, from: date)

      let datePath = ["\(year)", "\(Month(rawValue: month)!.string)", "\(day)"]

      print(datePath)

      return datePath
    }

    func run(with arguments: ArgumentParser.Result) throws {

      print("publishing missive")
      //
      // guard let stringPath = arguments.get(filename) else {
      //     throw "Bad path passed as argument"
      // }

      let draftName = try self.fileName(arguments)

      let postName  = [draftName[0].replacingOccurrences(of: ".md", with: ".html")]

      let workDir = try workingDirectory(arguments)

      let draftsDir = try self.draftsDirectory(arguments)

      let postsDir = try self.postsDirectory(arguments)

      let dateDir  = try self.dateDirectory(arguments)

      let fromPath = (workDir + draftsDir + draftName).joined(separator: "/")
      let toDirectory = (workDir + postsDir + dateDir).joined(separator: "/")
      let toPath   = (workDir + postsDir + dateDir + postName).joined(separator: "/")

      print("reading draft from \(fromPath)")

      print("writing post to \(toPath)")

      // let path = try validated(argument: argument)
      let source = try String(contentsOf: FileManager.makeFileURL(using: fromPath))
      let matter = try source.extractFrontMatter()
      let missive = Missive()
      let html = try missive.html(from: source.stripFrontMatter(), with: matter)
      // let toPath = FileManager.makeFileURL(using: Missive.defaultHTMLFile, relativeTo: writePath)
      try FileManager.default.createDirectory(atPath: toDirectory, withIntermediateDirectories: true, attributes: nil)

      try html.write(to: FileManager.makeFileURL(using: toPath), atomically: false, encoding: .utf8)

    }

}
