import Foundation
import Utility
import Basic
import MissiveKit

struct DraftCommand: Command {

    let command = "draft"
    let overview = "create a new blog post in markdown format"

    private let fileNameArgument: PositionalArgument<String>
    private let workDirOption: OptionArgument<String>
    private let draftsDirOption: OptionArgument<String>

    init(parser: ArgumentParser) {
        let subparser = parser.add(subparser: command, overview: overview)
        fileNameArgument = subparser.add(positional: "filename", kind: String.self, usage: "Name of the markdown file")
        workDirOption    = subparser.add(option: "--directory", shortName: "-wd", kind: String.self, usage: "Working directory for missive")
        draftsDirOption  = subparser.add(option: "--drafts", shortName: "-dd", kind: String.self, usage: "Drafts directory for missive")

    }

    func fileName(_ arguments: ArgumentParser.Result) throws -> [String] {

      guard let name = arguments.get(fileNameArgument) else {

        throw CommandLineError.missingFileName

      }

      return [name]

    }

    func workingDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      if let dir = arguments.get(workDirOption) {
          if FileManager.default.fileExists(atPath: dir) {
            return [dir]
          } else {
            throw CommandLineError.badWorkDirectoryName(dir)
          }
      } else {
        return []
      }

    }

    func draftsDirectory(_ arguments: ArgumentParser.Result) throws -> [String] {

      if let dir = arguments.get(draftsDirOption) {
        // print("draftsDir = \(draftsDir)")
        if FileManager.default.fileExists(atPath: dir) {
          return [dir]
        } else {
          throw CommandLineError.badDraftsDirectoryName(dir)
        }
      } else {
        return [Configuration.defaultDraftsDirectory]
      }
    }

    func run(with arguments: ArgumentParser.Result) throws {

      print("creating draft missive")

      let name = try self.fileName(arguments)

      let workDir = try workingDirectory(arguments)

      let draftsDir = try self.draftsDirectory(arguments)

      let pathVar = workDir + draftsDir + name

      let starter =
        """
        ---
        layout: post
        title:  Welcome to Missive!
        ---

        # Hello

        """

      let path = pathVar.joined(separator: "/")

      let urlPath = FileManager.makeFileURL(using: path)

      try starter.write(to: urlPath, atomically: false, encoding: .utf8)

      let availableAt = (draftsDir + name).joined(separator: "/")

      print("draft available at \(availableAt)")

    }

}
