//
//  String.extensions.swift
//  MissiveCLI
//
//  Created by Cavelle Benjamin on 19-Jan-10 (02).
//

import Foundation

extension String {
  func containsIgnoreCase(_ string: String) -> Bool {
    return self.lowercased().contains(string.lowercased())
  }
}
