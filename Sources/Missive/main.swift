import MissiveCLI

let missive = MissiveCommandLine()

if #available(macOS 10.12, *) {
  missive.run()
} else {
  print("need to run macOS 10.12")
}
