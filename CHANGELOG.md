# Missive Change Log
All notable changes to this project will be documented in this file.

* Format based on [Keep A Change Log](https://keepachangelog.com/en/1.0.0/)
* This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.2] - 2019-Jan-13 (build).
### Added
-

### Changed
- Updated CHANGELOG for 0.1.0 and 0.1.1 releases

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-

## [0.1.1] - 2019-Jan-13 (build).
### Added
- Docs for MissiveKit

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-

## [0.1.0] - 2019-Jan-13 (build).
### Added
- draft command to draft markdown posts. Defaults to drafts/ folder
- --drafts option to draft command for custom drafts folder
- publish command to publish markdown to html. Defaults to public/posts folder with YYYY/MMM/DD subfolder structure
- --posts option to publish command for custom posts folder

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-
