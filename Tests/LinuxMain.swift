import XCTest

import MissiveTests
import MissiveKitTests

var tests = [XCTestCaseEntry]()
tests += MissiveTests.allTests()
tests += MissiveKitTests.allTests()
XCTMain(tests)