
import XCTest
import MissiveKit
import DarkMatter

final class MissiveKitTests: XCTestCase {
  func testCompiles() {
      // This is an example of a functional test case.
      // Use XCTAssert and related functions to verify your tests produce the correct
      // results.
      XCTAssertEqual(Missive.defaultHTMLFile, "missive.html")
  }

  func testSimpleMarkdownToHTMLElement() {

    // given
    let missive = Missive()
    let expected = "<h1>Hello</h1>\n"

    // when
    let markdown = "# Hello"

    // then
    guard let actual = try? missive.render(markdown) else {
      XCTFail("Could not write markdown to html")
      return
    }

    XCTAssertEqual(actual, expected, "Missive did not write what was expected")

  }

  func testHMLT5Render() {
    // given
    let missive = Missive()
    let expected =
      """
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="UTF-8">
      <style type="text/css">
      pre {
          font-family: "Courier 10 Pitch", Courier, monospace;
          font-size: 95%;
          line-height: 140%;
          white-space: pre;
          white-space: pre-wrap;
          white-space: -moz-pre-wrap;
          white-space: -o-pre-wrap;
      }

      code {
          font-family: Monaco, Consolas, "Andale Mono", "DejaVu Sans Mono", monospace;
          font-size: 95%;
          line-height: 140%;
          white-space: pre;
          white-space: pre-wrap;
          white-space: -moz-pre-wrap;
          white-space: -o-pre-wrap;
          background: #faf8f0;
      }

      #content code {
          display: block;
          padding: 0.5em 1em;
          border: 1px solid #bebab0;
      }
      </style>
      <title></title>
      </head>
      <body>
      <h1>Hello</h1>
      <br/>
      <script src=\"https://gitlab.com/snippets/1818835.js\"></script>
      
      </body>
      </html>

      """

    // when
    let markdown =
    """
    # Hello
    <br/>
    <script src=\"https://gitlab.com/snippets/1818835.js\"></script>
    """

    // then
    guard let actual = try? missive.html(from: markdown) else {
      XCTFail("Could not write markdown to html")
      return
    }

    XCTAssertEqual(actual, expected, "Missive did not write what was expected")

  }

  func testFrontMatterExtraction() {

    // given
    let expectedStruct = FrontMatter(title: "Blogging Like a Hacker", layout: "post")

    let sut =
      """
      ---
      layout: post
      title: Blogging Like a Hacker
      ---

      # Hello

      <script src=\"https://gitlab.com/snippets/1818835.js\"></script>
      """

      // when
      guard let frontMatter = try? sut.extractFrontMatter() else {
        XCTFail("Could not get Front Matter from string")
        return
      }

      // then
      XCTAssertEqual(frontMatter, expectedStruct, "Front matter extraction did not go as planned")

  }

  func testStripFrontMatter() {

    // given
    let expected =
      """

      # Hello
      """

    let sut =
      """
      ---
      layout: post
      title: Blogging Like a Hacker
      ---

      # Hello
      """

      // when
      let actual = sut.stripFrontMatter()

      // then
      XCTAssertEqual(actual, expected, "Front matter not stripped as expected")
  }

  static var allTests = [
    ("testCompiles", testCompiles),
    ("testSimpleMarkdownToHTMLElement", testSimpleMarkdownToHTMLElement),
    ("testFrontMatterExtraction", testFrontMatterExtraction),
    ("testStripFrontMatter", testStripFrontMatter)
  ]
}
