import XCTest
import Foundation
import MissiveKit



final class MissiveTests: XCTestCase {

  enum Month: Int {
    case jan = 1
    case feb = 2
    case mar = 3
    case apr = 4
    case may = 5
    case jun = 6
    case jul = 7
    case aug = 8
    case sep = 9
    case oct = 10
    case nov = 11
    case dec = 12

    var string: String {
      switch self {
        case .jan: return "Jan"
        case .feb: return "Feb"
        case .mar: return "Mar"
        case .apr: return "Apr"
        case .may: return "May"
        case .jun: return "Jun"
        case .jul: return "Jul"
        case .aug: return "Aug"
        case .sep: return "Sep"
        case .oct: return "Oct"
        case .nov: return "Nov"
        case .dec: return "Dec"
      }
    }
  }

  /// Returns path to the built products directory.
  var productsDirectory: URL {
    #if os(macOS)
      for bundle in Bundle.allBundles where bundle.bundlePath.hasSuffix(".xctest") {
        return bundle.bundleURL.deletingLastPathComponent()
      }
      fatalError("couldn't find the products directory")
    #else
      return Bundle.main.bundleURL
    #endif
  }

  func contents(ofFile stringPath: String) throws -> String {

    let urlPath = FileManager.makeFileURL(using: stringPath)

    guard let content = try? String(contentsOf: urlPath) else {
      throw "Could not read file: \(stringPath)"
    }

    return content
  }

  func dateDirectory() -> [String] {

    let date = Date()
    let calendar = Calendar.current

    let year    = calendar.component(.year, from: date)
    let month    = calendar.component(.month, from: date)
    let day    = calendar.component(.day, from: date)
    // let hour    = calendar.component(.hour, from: date)
    // let minutes = calendar.component(.minute, from: date)
    // let seconds = calendar.component(.second, from: date)

    let datePath = ["\(year)", "\(Month(rawValue: month)!.string)", "\(day)"]

    print(datePath)

    return datePath
  }

  func run(_ arguments: [String], workingDirectory: String = "./", buildPath: String = "./.build") throws -> String {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct
    // results.

    // Some of the APIs that we use below are available in macOS 10.13 and above.
    guard #available(macOS 10.13, *) else {
        return "--- BAD RETURN ---"
    }

    let missive = Process()

    // let exacutable = FileManager.makeFileURL(using: "bin/missive")

    let exacutable = FileManager.makeFileURL(using: "/bin/bash")

    missive.executableURL = exacutable

    missive.arguments = ["-c", "cd \(workingDirectory) && swift run --build-path \(buildPath) missive \(arguments.joined(separator: " "))"]

    print(String(describing: missive.arguments))

    let stdOut = Pipe()
    missive.standardOutput = stdOut

    let stdErr = Pipe()
    missive.standardError = stdErr

    #if os(Linux)
      missive.launch()
    #else
      try missive.run()
    #endif

    // try process.run()
    missive.waitUntilExit()

    let data = stdOut.fileHandleForReading.readDataToEndOfFile()

    guard let output = String(data: data, encoding: .utf8) else {
      return "--- BAD RETURN ---"
    }

    let errData = stdErr.fileHandleForReading.readDataToEndOfFile()

    if let errOutput = String(data: errData, encoding: .utf8) {
      print(errOutput)
    }

    return output
  }

  func testNoArguments() {

    // given
    let expected  =
    """
    OVERVIEW: Write blog posts in markdown, publish to html


    USAGE: missive <command> <options>

    SUBCOMMANDS:
      draft                   create a new blog post in markdown format
      publish                 Publish a markdown formatted post to html

    """

    let arguments = [String]()

    // when
    guard let actual = try? run(arguments) else {
      XCTFail("Could not run [missive] with arguments: \(arguments)")
      return
    }

    // then
    XCTAssertEqual(actual, expected, "output from missive not as expected")

  }

  func testHelpArgument() {

    // given
    let expected  =
    """
    OVERVIEW: Write blog posts in markdown, publish to html


    USAGE: missive <command> <options>

    SUBCOMMANDS:
      draft                   create a new blog post in markdown format
      publish                 Publish a markdown formatted post to html

    """

    let arguments = ["--help"]

    // when
    guard let actual = try? run(arguments) else {
      XCTFail("Could not run [missive] with arguments: \(arguments)")
      return
    }

    // then
    XCTAssertEqual(actual, expected, "output from missive not as expected")

  }

  func testDraftCommandDefaultConfigs() {

    // given
    let stringPath = "draft.md"
    let outputPath = "Tests/fixtures/example/drafts/draft.md"
    let expected  =
      """
      ---
      layout: post
      title:  Welcome to Missive!
      ---

      # Hello

      """

    let arguments = ["draft \(stringPath)"]

    // when
    guard let output = try? run(arguments, workingDirectory: "Tests/fixtures/example", buildPath: "../../../.build") else {
      XCTFail("Could not run [missive] with arguments: \(arguments)")
      return
    }

    print(output)

    guard let actual = try? contents(ofFile: outputPath) else {
      XCTFail("Could not read file '\(stringPath)'")
      return
    }

    // then
    XCTAssertEqual(actual, expected, "output from missive not as expected")

  }

  func testPublishCommandDefaultConfigs() {
    // given
    let draftSource = "Tests/fixtures/example/drafts/draft.md"
    let draftPath  = "draft.md"
    let publishedPath = (["Tests", "fixtures", "example", "public", "posts"] + dateDirectory() +  ["draft.html"]).joined(separator: "/")

    let draftString  =
      """
      ---
      layout: post
      title:  Welcome to Missive!
      ---

      # Hello

      """

    let expected  =
    """
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="UTF-8">
    <style type="text/css">
    pre {
        font-family: "Courier 10 Pitch", Courier, monospace;
        font-size: 95%;
        line-height: 140%;
        white-space: pre;
        white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -o-pre-wrap;
    }

    code {
        font-family: Monaco, Consolas, "Andale Mono", "DejaVu Sans Mono", monospace;
        font-size: 95%;
        line-height: 140%;
        white-space: pre;
        white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -o-pre-wrap;
        background: #faf8f0;
    }

    #content code {
        display: block;
        padding: 0.5em 1em;
        border: 1px solid #bebab0;
    }
    </style>
    <title>Welcome to Missive!</title>
    </head>
    <body>
    <h1>Hello</h1>

    </body>
    </html>

    """

    let arguments = ["publish \(draftPath)"]

    if !FileManager.default.fileExists(atPath: draftSource) {
      guard let _ = try? draftString.write(to: FileManager.makeFileURL(using: draftSource), atomically: false, encoding: .utf8) else {
        XCTFail("Could not write draft string to draft folder: \(draftString) \n \(draftSource)")
        return
      }
    }

    // when
    guard let output = try? run(arguments, workingDirectory: "Tests/fixtures/example", buildPath: "../../../.build") else {
      XCTFail("Could not run [missive] with arguments: \(arguments)")
      return
    }

    print(output)

    guard let actual = try? contents(ofFile: publishedPath) else {
      XCTFail("Could not read file '\(publishedPath)'")
      return
    }

    // then
    XCTAssertEqual(actual, expected, "output from missive not as expected")

  }

  static var allTests = [
    ("testNoArguments", testNoArguments),
    ("testHelpArgument", testHelpArgument),
    ("testDraftCommandDefaultConfigs", testDraftCommandDefaultConfigs),
    ("testPublishCommandDefaultConfigs", testPublishCommandDefaultConfigs)
  ]
}
