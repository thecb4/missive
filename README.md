# Missive

Write blog posts in markdown, publish to html

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

---
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [SwiftMarkdown](https://github.com/vapor-community/markdown) - Swift wrapper for cmark. Written by the folks at [Vapor](https://vapor.codes).
* [DarkMatter](https://gitlab.com/thecb4/DarkMatter.git) - JSON Coder extensions

### Prerequisites

Must have [Swift](https://swift.org) installed.

### Installing

Clone and install from script

```
git clone https://gitlab.com/thecb4/missive.git
cd missive
bin/install.sh
```
----
## Using

Missive assumes you have a certain directory structure


![alt text](docs/assets/images/directory-structure.png "Directory Structure")

### Help

A list of all missive commands can be obtained by using the --help option
```
missive --help
```

### Drafts

Starting a draft is very simple. Drafts are automatically stored in the drafts/ folder of the current directory (if it exists).

```
missive draft something.md
```

Specifying a drafts directory is possible with the --drafts option

```
missive draft something.md --drafts custom-drafts-folder
```

Drafts limited support for YAML front matter
```
---
title: Blogging Like a Hacker
---
```

Currently supported Front Matter:
* title - adds the given title to the `<title></title>` tag

### Publishing

Publishing a draft is very simple. Drafts are published as HTML to the public/posts/ folder with a YYYY/MMM/DD/ structure automatically created.

```
missive publish something.md
```

Specifying a posts directory is possible with the --posts option

```
missive publish something.md --posts custom-posts-folder
```
---

## Developing with MissiveKit

Developing with MissiveKit is straight forward

### MissiveKit Dependency
```
.package(url: "https://gitlab.com/thecb4/missive.git", .upToNextMinor(from: "0.1.0"))
```

```
import MissiveKit
```

### Documentation

Documentation for MissiveKit can be found [here](https://thecb4.gitlab.io/missive)

---
## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library.


## Authors

* **Cavelle Benjamin** - *Initial work* - [thecb4](https://thecb4.io)


## Acknowledgments

* Hat tip to anyone whose code was used
