// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Missive",
    products: [
      .executable(name: "missive", targets: ["Missive"]),
      .library(name: "MissiveCLI", targets: ["MissiveCLI"]),
      .library(name: "MissiveKit", targets: ["MissiveKit"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
      .package(url: "https://github.com/vapor-community/markdown.git", .upToNextMinor(from: "0.4.0")),
      .package(url: "https://github.com/apple/swift-package-manager.git", .branch("swift-4.2-branch")),
      .package(url: "https://gitlab.com/thecb4/DarkMatter.git", .upToNextMinor(from: "0.2.2"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "Missive",
            dependencies: ["MissiveCLI"]),
        .target(
            name: "MissiveCLI",
            dependencies: ["MissiveKit", "Utility"]),
        .target(
            name: "MissiveKit",
            dependencies: ["SwiftMarkdown", "DarkMatter"]),
        .testTarget(
            name: "MissiveKitTests",
            dependencies: ["MissiveKit", "DarkMatter"]),
        .testTarget(
            name: "MissiveTests",
            dependencies: ["Missive", "MissiveKit"])
    ]
)
