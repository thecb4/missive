#!/usr/bin/env bash

swift build && \
sourcekitten doc --spm-module MissiveKit > source.json && \
jazzy && \
rm source.json
